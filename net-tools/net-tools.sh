#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : net-tools.sh
# * Author     : 苏木
# * Date       : 2024-09-29
# * ======================================================
##

#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : net_tool.sh
# * Author     : 苏木
# * Date       : 2023-07-01
# * ======================================================
##

BLACK="\033[1;30m"
RED='\033[1;31m'    # 红
GREEN='\033[1;32m'  # 绿
YELLOW='\033[1;33m' # 黄
BLUE='\033[1;34m'   # 蓝
PINK='\033[1;35m'   # 紫
CYAN='\033[1;36m'   # 青
WHITE='\033[1;37m'  # 白
CLS='\033[0m'       # 清除颜色

INFO="${GREEN}[INFO]${CLS}"
WARN="${YELLOW}[WARN]${CLS}"
ERR="${RED}[ERR ]${CLS}"

SCRIPT_NAME=${0#*/}
SCRIPT_CURRENT_PATH=${0%/*}
SCRIPT_ABSOLUTE_PATH=`cd $(dirname ${0}); pwd`

SYSTEM_ENVIRONMENT_FILE=/etc/profile # 系统环境变量位置
USER_ENVIRONMENT_FILE=~/.bashrc
SOFTWARE_DIR_PATH=~/2software        # 软件安装目录
# * ======================================================

# 打印菜单
function net_tool_echo_menu()
{
	echo "================================================="
	echo -e "${GREEN}               网络工具安装与配置 ${CLS}"
	echo "================================================="
	echo -e "${PINK}current path    :$(pwd)${CLS}"
	echo ""
	echo -e "${GREEN} *[0]${CLS} net tools"
	echo -e "${GREEN} *[1]${CLS} 更换阿里云软件源"
	echo -e "${GREEN} *[2]${CLS} 网络图标消失解决"
	echo ""
	echo "================================================="
}

# 安装 net-tools 
function net_tools_install()
{
    cd ~
    echo -e "${INFO}current path:$(pwd)"
	if ! $(command -v ifconfig >/dev/null 2>&1) ; then
		echo -e "${ERR}net-tools 尚未安装,准备在线安装net-tools..."
        sudo apt-get update
        sudo apt install -y net-tools
	else
		echo -e "${INFO}net-tools 已安装,本机IP信息如下:"
    fi
	ifconfig
}

# 备份现有的源列表
function source_list_backup() 
{
    echo -e ${INFO}"备份现有软件源..."
    sudo cp -pvf /etc/apt/sources.list /etc/apt/sources.list.bak
    echo -e ${INFO}"备份完毕..."
}

# 添加华为源和中国科技大学源
function source_list_add() 
{
    echo -e ${INFO}"添加华为源和中国科技大学源(会覆盖原有的源)..."
    sudo bash -c 'cat <<EOF > /etc/apt/sources.list
deb https://repo.huaweicloud.com/ubuntu/ jammy main restricted universe multiverse
deb https://repo.huaweicloud.com/ubuntu/ jammy-updates main restricted universe multiverse
deb https://repo.huaweicloud.com/ubuntu/ jammy-backports main restricted universe multiverse
deb https://repo.huaweicloud.com/ubuntu/ jammy-security main restricted universe multiverse

deb https://mirrors.ustc.edu.cn/ubuntu/ jammy main restricted universe multiverse
deb https://mirrors.ustc.edu.cn/ubuntu/ jammy-updates main restricted universe multiverse
deb https://mirrors.ustc.edu.cn/ubuntu/ jammy-backports main restricted universe multiverse
deb https://mirrors.ustc.edu.cn/ubuntu/ jammy-security main restricted universe multiverse
EOF'
    
    echo -e ${INFO}"添加完毕..."
}

# 更新软件包列表
function update_package_list() 
{
    echo -e ${INFO}"正在更新包列表..."
    sudo apt-get update
    echo -e ${INFO}"更新完毕..."
}

# 更更换软件源
function source_list_change()
{
    echo -e ${INFO}"更换软件源为阿里云软件源..."
    sudo cp -pvf ./etc/apt/ubuntu-20.04.2-desktop-sources.list /etc/apt/sources.list
    echo -e ${INFO}"更换完毕..."
    update_package_list
}

# 解决网络图标消失问题(网卡消失)
function net_icon_solution()
{
    sudo service network-manager stop
    sudo rm /var/lib/NetworkManager/NetworkManager.state 
    sudo service network-manager start
}

function net_tool_func_process()
{
    net_tool_echo_menu
	read -p "请选择功能，默认选择退出:" choose
	case "${choose}" in
		"0") net_tools_install;;
		"1") source_list_change;;
		"2") net_icon_solution;;
		*)  exit 0;;
	esac
}

net_tool_func_process
