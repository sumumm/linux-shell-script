#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : develop_tool.sh
# * Author     : 苏木
# * Date       : 2024-09-21
# * ======================================================
##
BLACK="\033[1;30m"
RED='\033[1;31m'    # 红
GREEN='\033[1;32m'  # 绿
YELLOW='\033[1;33m' # 黄
BLUE='\033[1;34m'   # 蓝
PINK='\033[1;35m'   # 紫
CYAN='\033[1;36m'   # 青
WHITE='\033[1;37m'  # 白
CLS='\033[0m'       # 清除颜色

INFO="${GREEN}[INFO]${CLS}"
WARN="${YELLOW}[WARN]${CLS}"
ERR="${RED}[ERR ]${CLS}"

SCRIPT_NAME=${0#*/}
SCRIPT_CURRENT_PATH=${0%/*}
SCRIPT_ABSOLUTE_PATH=`cd $(dirname ${0}); pwd`

SYSTEM_ENVIRONMENT_FILE=/etc/profile # 系统环境变量位置
USER_ENVIRONMENT_FILE=~/.bashrc
SOFTWARE_DIR_PATH=~/2software        # 软件安装目录
# * ======================================================
# 打印菜单
function dev_tool_echo_menu()
{
	echo "================================================="
	echo -e "${GREEN}               基本开发工具 ${CLS}"
	echo "================================================="
	echo -e "${PINK}current path        :$(pwd)${CLS}"
    echo -e "${PINK}SCRIPT_ABSOLUTE_PATH:${SCRIPT_ABSOLUTE_PATH}${CLS}"
    echo -e "${PINK}SHARED_DIR_PATH     :${SHARED_DIR_PATH}${CLS}"
	echo ""
	echo -e "${GREEN} *[0]${CLS} 在线安装GCC"
    echo -e "${GREEN} *[1]${CLS} 在线安装make"
    echo -e "${GREEN} *[2]${CLS} 本地安装arm-linux-gnueabihf"
    echo -e "${GREEN} *[3]${CLS} 安装免安装版本VScode"
    echo -e "${GREEN} *[4]${CLS} 安装基本开发工具(一步到位)"
	echo ""
	echo "================================================="
}

function gcc_install_online()
{

    cd ~
    echo -e "${INFO}${PINK}current path:$(pwd)${CLS}"
	
	if ! $(command -v gcc >/dev/null 2>&1) ; then
    	echo -e "${ERR}gcc 尚未安装,准备在线安装gcc..."
        sudo apt install gcc
		echo -e "${INFO}安装完毕,当前gcc版本如下:"
    else
        echo -e "${INFO}gcc 已安装,当前gcc版本如下:"
    fi
	
	gcc_version=$(gcc --version | sed -n "1p")
    echo ${gcc_version}
}

function make_install_online()
{

    cd ~
    echo -e "${INFO}${PINK}current path:$(pwd)${CLS}"
	
	if ! $(command -v make >/dev/null 2>&1) ; then
    	echo -e "${ERR}make 尚未安装,准备在线安装make..."
        sudo apt install make
		echo -e "${INFO}安装完毕,当前make版本如下:"
    else
        echo -e "${INFO}make 已安装,当前make版本如下:"
    fi
	
	make_version=$(make --version | sed -n "1p")
    echo ${make_version}
}

# 安装 arm-linux-gnueabihf-gcc
function arm_linux_gnueabihf_install()
{
	GNUEABIHF_GCC_VERSION_NAME=gcc-linaro-4.9.4
	GNUEABIHF_GCC_PACK_NAME=gcc-linaro-4.9.4-2017.01-x86_64_arm-linux-gnueabihf.tar.xz
    GNUEABIHF_GCC_PACK_PATH=${SCRIPT_ABSOLUTE_PATH}/arm-linux-gcc

	GNUEABIHF_GCC_INSTALL_PATH=${SOFTWARE_DIR_PATH}/${GNUEABIHF_GCC_VERSION_NAME}

    cd ~
    echo -e "${INFO}${PINK}current path               :$(pwd)${CLS}"
	echo -e "${INFO}${PINK}GNUEABIHF_GCC_VERSION_NAME :${GNUEABIHF_GCC_VERSION_NAME}${CLS}"
	echo -e "${INFO}${PINK}GNUEABIHF_GCC_PACK_NAME    :${GNUEABIHF_GCC_PACK_NAME}${CLS}"
	echo -e "${INFO}${PINK}GNUEABIHF_GCC_PACK_PATH    :${GNUEABIHF_GCC_PACK_PATH}${CLS}"
	echo -e "${INFO}${PINK}GNUEABIHF_GCC_INSTALL_PATH :${GNUEABIHF_GCC_INSTALL_PATH}${CLS}"
	
	if ! $(command -v arm-linux-gnueabihf-gcc >/dev/null 2>&1) ; then
    	echo -e "${ERR}arm-linux-gnueabihf-gcc 尚未安装,准备安装arm-linux-gnueabihf-gcc..."
		echo -e "${INFO}安装相关依赖..."
		sudo apt-get install -y lsb-core lib32stdc++6
		sleep 2s
		echo -e "${INFO}创建安装目录..."
		if [ -d "${GNUEABIHF_GCC_INSTALL_PATH}" ];then
			echo -e "${WARN}${GNUEABIHF_GCC_INSTALL_PATH}目录已存在!!!"
			read -p "是否删除?(默认选择删除继续安装，退出安装输入n):" ret
			if [ "${ret}" != "n" ];then
				sudo rm -rf ${GNUEABIHF_GCC_INSTALL_PATH}
			else
				return
			fi
		else
			echo -e "${INFO}即将创建${GNUEABIHF_GCC_VERSION_NAME}安装目录..."
		fi
		sudo mkdir -pv ${GNUEABIHF_GCC_INSTALL_PATH}
	
		echo -e "${INFO}下载 arm-linux-gnueabihf-gcc(v4.9.4 x86_64) 安装包..."
		if [ "$(command -v wget >/dev/null 2>&1)" ] ; then
			echo -e "${ERR}wget 尚未安装,请先安装wget!!!"
			return
		else
			echo -e "${INFO}wget 已安装,当前wget版本如下:"
			wget_version=$(wget --version | sed -n "1p")
			echo ${wget_version}
		fi
		mkdir -pv ~/gnueabihf_gcc_temp
		#wget -P ~/gnueabihf_gcc_temp/ https://publishing-ie-linaro-org.s3.amazonaws.com/releases/components/toolchain/binaries/4.9-2017.01/arm-linux-gnueabihf/gcc-linaro-4.9.4-2017.01-x86_64_arm-linux-gnueabihf.tar.xz?Signature=QmTIDeXj27Ah00GOyt9AebankSk%3D&Expires=1682730917&AWSAccessKeyId=AKIAIELXV2RYNAHFUP7A
		if [ ! -e "${GNUEABIHF_GCC_PACK_PATH}/${GNUEABIHF_GCC_PACK_NAME}" ];then
			echo -e "${ERR}本地安装包 ${GNUEABIHF_GCC_PACK_NAME} 不存在!!!"
			echo -e "${INFO}可以在这里下载: https://releases.linaro.org/components/toolchain/binaries/4.9-2017.01/arm-linux-gnueabihf/ !!!"
			echo -e "${INFO}开始在线下载 ${GNUEABIHF_GCC_PACK_NAME}"
			wget -P ~/gnueabihf_gcc_temp https://releases.linaro.org/components/toolchain/binaries/4.9-2017.01/arm-linux-gnueabihf/gcc-linaro-4.9.4-2017.01-x86_64_arm-linux-gnueabihf.tar.xz
		else
			echo -e "${INFO}本地安装包 ${GNUEABIHF_GCC_PACK_NAME} 存在，优先使用本地安装包安装"
			cp -pv ${GNUEABIHF_GCC_PACK_PATH}/${GNUEABIHF_GCC_PACK_NAME} ~/gnueabihf_gcc_temp
		fi
		pack_path=$(find ~/gnueabihf_gcc_temp -name '*.tar.xz')
		if [ -z ${pack_path} ];then
			echo -e "${ERR}无可用安装包!!!"
			rm -rf ~/gnueabihf_gcc_temp
			return
		fi
		sudo tar -xf ${pack_path} --strip-components 1 -C ${GNUEABIHF_GCC_INSTALL_PATH}
		rm -rf ~/gnueabihf_gcc_temp
		
		echo -e "${INFO}添加环境变量..."
		local ARM_GCC_FIND_STR="${GNUEABIHF_GCC_INSTALL_PATH}/bin"

		# 判断匹配函数，匹配函数不为0，则包含给定字符
		ENVIRONMENT_FILE_PATH=${USER_ENVIRONMENT_FILE}
		if [ `grep -c "${ARM_GCC_FIND_STR}" ${ENVIRONMENT_FILE_PATH}` -ne '0' ];then
			echo -e "${INFO}文件中已包含 ${ARM_GCC_FIND_STR}，不需要重新写入!"
		else
			environment_data="export PATH=\$PATH:${GNUEABIHF_GCC_INSTALL_PATH}/bin"
			if test -s ${ENVIRONMENT_FILE_PATH} ;then # 文件存在且至少有一个字符为真
				sudo sed -i "\$a $environment_data" ${ENVIRONMENT_FILE_PATH}
			else
				echo "$environment_data" >> ${ENVIRONMENT_FILE_PATH}
			fi
			source ${ENVIRONMENT_FILE_PATH}
			echo -e "${INFO}安装完毕(为确保后续没有问题，请重启ubuntu)..."
		fi
    else
        echo -e "${INFO}arm-linux-gnueabihf-gcc 已安装,当前arm-linux-gnueabihf-gcc版本如下:"
    fi
	
	# 这里有可能出现未找到命令的情况，重启下终端即可
	gnueabihf_gcc_version=$(arm-linux-gnueabihf-gcc --version | sed -n "1p")
    echo ${gnueabihf_gcc_version}
    
}

# 安装vscode
function vscode_install_local()
{
    VSCODE_DIR_NAME=VSCode-linux-x64
    VSCODE_DIR_PATH=${SOFTWARE_DIR_PATH}/${VSCODE_DIR_NAME}
	VSCODE_SHORTCUT_NAME=VScode.desktop
    VSCODE_SHORTCUT_PATH=/usr/share/applications/

	SHORTCUT_DATA=( "[Desktop Entry]"
					"Name=VScode"
					"Comment=Multi-platform code editor for Linux"
					"Exec=${VSCODE_DIR_PATH}/code"
					"Icon=${VSCODE_DIR_PATH}/resources/app/resources/linux/code.png"
					"Type=Application"
					"StartupNotify=true")

    cd ~
    echo -e "${INFO}${PINK}current path   :$(pwd)${CLS}"
    echo -e "${INFO}${PINK}VSCODE_DIR_NAME:${VSCODE_DIR_NAME}${CLS}"
    echo -e "${INFO}${PINK}VSCODE_DIR_PATH:${VSCODE_DIR_PATH}${CLS}"
    echo -e "${INFO}${PINK}VSCODE_SHORTCUT:${VSCODE_SHORTCUT_PATH}/${VSCODE_SHORTCUT_NAME}${CLS}"
	
	if [ -d "${VSCODE_DIR_PATH}" ];then
		echo -e "${WARN}vscode目录已存在!!!"
		read -p "是否重新安装?(默认选择重新安装，退出输入n):" ret
		if [ "${ret}" != "n" ];then
			sudo rm -rf ${VSCODE_DIR_PATH}
		else
			return
		fi
    else
		echo -e "${INFO}即将创建vscode安装目录..."
    fi
	sudo mkdir -pv ${VSCODE_DIR_PATH}
	
    if [ "$(command -v wget >/dev/null 2>&1)" ] ; then
    	echo -e "${ERR}wget 尚未安装,请先安装wget!!!"
        return
    else
        echo -e "${INFO}wget 已安装,当前wget版本如下:"
        wget_version=$(wget --version | sed -n "1p")
        echo ${wget_version}
    fi
	
    mkdir -pv ~/vscode_temp
    wget -P ~/vscode_temp/ https://az764295.vo.msecnd.net/stable/704ed70d4fd1c6bd6342c436f1ede30d1cff4710/code-stable-x64-1681293081.tar.gz
	
    pack_path=$(find ~/vscode_temp -name '*.tar.gz')
    sudo tar -zxvf ${pack_path} --strip-components 1 -C ${VSCODE_DIR_PATH}
    rm -rf ~/vscode_temp
	
    sudo chmod +x ${VSCODE_DIR_PATH}/code

    touch ${VSCODE_SHORTCUT_NAME}
	for data in "${SHORTCUT_DATA[@]}"
		do
			echo "${data}"
			if test -s ${VSCODE_SHORTCUT_NAME} ;then # 文件存在且至少有一个字符为真
				sed -i "\$a $data" ${VSCODE_SHORTCUT_NAME}
			else
				echo "$data" >> ${VSCODE_SHORTCUT_NAME}
			fi
		done
	sudo mv -v ${VSCODE_SHORTCUT_NAME} ${VSCODE_SHORTCUT_PATH}
}

function dev_tool_install()
{
    gcc_install_online
    make_install_online
    arm_linux_gnueabihf_install
    #vscode_install_local
}

function dev_tool_func_process()
{
    dev_tool_echo_menu
	read -p "请选择功能，默认选择退出:" choose
	case "${choose}" in
		"0") gcc_install_online;;
		"1") make_install_online;;
		"2") arm_linux_gnueabihf_install;;
        "3") vscode_install_local;;
        "4") dev_tool_install;;
		*)  exit 0;;
	esac
}

dev_tool_func_process