#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : vim.sh
# * Author     : 苏木
# * Date       : 2024-09-28
# * ======================================================
##

BLACK="\033[1;30m"
RED='\033[1;31m'    # 红
GREEN='\033[1;32m'  # 绿
YELLOW='\033[1;33m' # 黄
BLUE='\033[1;34m'   # 蓝
PINK='\033[1;35m'   # 紫
CYAN='\033[1;36m'   # 青
WHITE='\033[1;37m'  # 白
CLS='\033[0m'       # 清除颜色

INFO="${GREEN}[INFO]${CLS}"
WARN="${YELLOW}[WARN]${CLS}"
ERR="${RED}[ERR ]${CLS}"

SCRIPT_NAME=${0#*/}
SCRIPT_CURRENT_PATH=${0%/*}
SCRIPT_ABSOLUTE_PATH=`cd $(dirname ${0}); pwd`

SYSTEM_ENVIRONMENT_FILE=/etc/profile # 系统环境变量位置
USER_ENVIRONMENT_FILE=~/.bashrc
SOFTWARE_DIR_PATH=~/2software        # 软件安装目录
# * ======================================================

# 打印菜单
function vim_echo_menu()
{
	echo "================================================="
	echo -e "${GREEN}               VIM安装与配置 ${CLS}"
	echo "================================================="
	echo -e "${PINK}current path    :$(pwd)${CLS}"
	echo ""
	echo -e "${GREEN} *[0]${CLS} 在线安装VIM"
    echo -e "${GREEN} *[1]${CLS} 对VIM进行配置"
	echo ""
	echo "================================================="
}

function vim_install_online()
{
    cd ~
    echo -e "${INFO}${PINK}current path:$(pwd)${CLS}"
	if ! $(command -v vim >/dev/null 2>&1) ; then
		echo -e "${ERR}vim 尚未安装,准备在线安装vim..."
        sudo add-apt-repository -y ppa:jonathonf/vim # ubuntu-tweak官方源
		sudo apt update       # 更新源
		sudo apt install -y vim  # 安装vim
		echo -e "${INFO}安装完毕,当前vim版本如下:"
	else
		echo -e "${INFO}vim 已安装,当前vim版本如下:"
    fi

    vim_version=$(vim --version | sed -n "1p")
    echo ${vim_version} 
}

# 配置vim
function vim_config()
{
    cd ~
    echo -e "${INFO}${PINK}current path:$(pwd)${CLS}"
	echo -e "${ERR}暂不支持配置,敬请期待!!!"
}

function vim_func_process()
{
    vim_echo_menu
	read -p "请选择功能，默认选择退出:" choose
	case "${choose}" in
		"0") vim_install_online;;
		"1") vim_config;;
		*)  exit 0;;
	esac
}

vim_func_process