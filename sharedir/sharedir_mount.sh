#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : sharedir_mount.sh
# * Author     : sumu
# * Date       : 2024-09-20
# * ======================================================
##
BLACK="\033[1;30m"
RED='\033[1;31m'    # 红
GREEN='\033[1;32m'  # 绿
YELLOW='\033[1;33m' # 黄
BLUE='\033[1;34m'   # 蓝
PINK='\033[1;35m'   # 紫
CYAN='\033[1;36m'   # 青
WHITE='\033[1;37m'  # 白
CLS='\033[0m'       # 清除颜色

INFO="${GREEN}[INFO]${CLS}"
WARN="${YELLOW}[WARN]${CLS}"
ERR="${RED}[ERR ]${CLS}"

SCRIPT_NAME=${0#*/}
SCRIPT_CURRENT_PATH=${0%/*}
SCRIPT_ABSOLUTE_PATH=`cd $(dirname ${0}); pwd`

SYSTEM_ENVIRONMENT_FILE=/etc/profile # 系统环境变量位置
USER_ENVIRONMENT_FILE=~/.bashrc
SOFTWARE_DIR_PATH=~/2software        # 软件安装目录

# ubuntu20.04挂载共享目录（在ubuntu中手动敲下边的命令）
SHARE_DIR_MOUNT_PATH=/mnt/hgfs
SHARE_DIR_NAME=
SHARED_DIR_PATH=

function shared_dir_mount()
{
    if [ ! -d ${SHARE_DIR_MOUNT_PATH} ];then
	    sudo mkdir -pv ${SHARE_DIR_MOUNT_PATH}
    	sudo chmod 777 ${SHARE_DIR_MOUNT_PATH}
    else
    	echo "${SHARE_DIR_MOUNT_PATH} 路径已存在..."
    fi
    
    vmware-hgfsclient     # 查看vmware设置的共享文件夹名
    
}

# 获取共享目录的路径
function share_dir_path_get()
{
    # /mnt/hgfs 目录
    if [ ! -d ${SHARE_DIR_MOUNT_PATH} ];then
	    sudo mkdir -pv ${SHARE_DIR_MOUNT_PATH}
    	sudo chmod 777 ${SHARE_DIR_MOUNT_PATH}
        echo -e "${INFO}${SHARE_DIR_MOUNT_PATH} 路径已创建..."
    else
    	echo -e "${INFO}${SHARE_DIR_MOUNT_PATH} 路径已存在..."
    fi
    # 检查是否有共享文件夹存在
	SHARE_DIR_NAME=$(vmware-hgfsclient)
    if [ -n "${SHARE_DIR_NAME}" ];then
        SHARED_DIR_PATH=${SHARE_DIR_MOUNT_PATH}/${SHARE_DIR_NAME}
        echo -e "${INFO}shared_dir path is ${SHARED_DIR_PATH} !!!"
    else
        echo -e "${ERR}被禁止,无法进行后续工作,请检查VMware虚拟机是否开启了共享目录功能!!!"
        return
    fi
    # 开始挂载共享目录
    if [ ! -d ${SHARED_DIR_PATH} ];then
        sudo vmhgfs-fuse .host:/ /mnt/hgfs/ -o allow_other # 挂载共享文件夹
    else
        echo -e "${INFO}${SHARED_DIR_PATH}已挂载... !!!"
    fi
}
# 配置共享目录
function shared_dir_config()
{
    if [ ! -n "${SHARED_DIR_PATH}" ];then
        echo -e "${ERR}SHARED_DIR_PATH is NULL!!!"
        return
    else
        echo -e "${PINK}SHARED_DIR_PATH is ${SHARED_DIR_PATH} ${CLS}"
    fi

    local mount_sharedir_path=${SHARE_DIR_MOUNT_PATH}
	local fstab_path=/etc/fstab
    local shared_dir_ln[0]=~/1sharedfiles
    local shared_dir_ln[1]=~/桌面/1sharedfiles

    cd ~
    echo -e "${INFO}${PINK}current path    :$(pwd)${CLS}"
	echo -e "${INFO}${PINK}fstab_path      :${fstab_path}${CLS}"
    echo -e "${INFO}${PINK}SHARED_DIR_PATH :${SHARED_DIR_PATH}${CLS}"
    echo -e "${INFO}${PINK}SHARED_DIR_NAME :${SHARED_DIR_PATH##*/}${CLS}"
    
    read -p "是否进行设置开机自动挂载?(默认选择不进行开机自动挂载,需要请输入y):" ret
    if [ "${ret}" == "y" ];then
        # 开机自动挂载
        echo -e "${INFO}设置开机自动挂载..."
        local find_str="fuse.vmhgfs-fuse"
        # 判断匹配函数，匹配函数不为0，则包含给定字符
        if [ `grep -c "${find_str}" ${fstab_path}` -ne '0' ];then
            echo -e "${INFO}文件中已包含 ${find_str}，不需要重新写入!"
        else
            local auto_mount_command=".host:/ ${mount_sharedir_path} fuse.vmhgfs-fuse allow_other,defaults 0 0"
            if test -s ${fstab_path} ;then # 文件存在且至少有一个字符为真
                sudo sed -i "\$a ${auto_mount_command}" ${fstab_path}
            else
                sudo echo "${auto_mount_command}" >> ${fstab_path}
            fi
        fi
    fi
	
    # 创建一些软链接，方便使用
    read -p "是否创建软链接?(默认选择不创建,需要请输入y):" ret
    if [ "${ret}" == "y" ];then
        
        echo -e "${INFO}创建软链接方便使用..."
        for temp in ${shared_dir_ln[@]}
        do
            if [ -e "${temp}" ];then
                echo -e "${WARN}${temp}文件已存在,可以运行命令:【rm ${temp}】,删除后重试!!!"
            else
                ln -s ${SHARED_DIR_PATH} ${temp}
            fi
        done
        for temp in ${shared_dir_ln[@]}
        do
            if [ -L "${temp}" ];then
                echo -e "${INFO}${temp}软链接可以正常使用!!!"
            else
                echo -e "${ERR}${temp}软链接无法正常使用!!!"
            fi
        done
    fi
}


share_dir_path_get
shared_dir_config
