#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : ssh.sh
# * Author     : 苏木
# * Date       : 2024-09-28
# * ======================================================
##

BLACK="\033[1;30m"
RED='\033[1;31m'    # 红
GREEN='\033[1;32m'  # 绿
YELLOW='\033[1;33m' # 黄
BLUE='\033[1;34m'   # 蓝
PINK='\033[1;35m'   # 紫
CYAN='\033[1;36m'   # 青
WHITE='\033[1;37m'  # 白
CLS='\033[0m'       # 清除颜色

INFO="${GREEN}[INFO]${CLS}"
WARN="${YELLOW}[WARN]${CLS}"
ERR="${RED}[ERR ]${CLS}"

SCRIPT_NAME=${0#*/}
SCRIPT_CURRENT_PATH=${0%/*}
SCRIPT_ABSOLUTE_PATH=`cd $(dirname ${0}); pwd`

SYSTEM_ENVIRONMENT_FILE=/etc/profile # 系统环境变量位置
USER_ENVIRONMENT_FILE=~/.bashrc
SOFTWARE_DIR_PATH=~/2software        # 软件安装目录

# 安装ssh-server
function ssh_server_install_online()
{
    cd ~
    echo -e "${INFO}current path:$(pwd)"
	ssh_info=$(ps -e | grep ssh | grep "sshd")
	if [ "${ssh_info}" == "" ];then
		echo -e "${ERR}ssh-server 尚未安装,准备安装 ssh-server..."
		sudo apt-get install -y openssh-server
		sudo /etc/init.d/ssh start
		echo -e "${INFO}ssh-server 安装完毕,信息如下:"
	else
		echo -e "${INFO}ssh-server 已安装,信息如下:"
	fi
	
	ssh_info=$(ps -e | grep ssh | grep "sshd")
	echo $ssh_info
}

ssh_server_install_online