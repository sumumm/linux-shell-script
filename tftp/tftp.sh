#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : tftp.sh
# * Author     : 苏木
# * Date       : 2024-09-21
# * ======================================================
##
# * ======================================================

BLACK="\033[1;30m"
RED='\033[1;31m'    # 红
GREEN='\033[1;32m'  # 绿
YELLOW='\033[1;33m' # 黄
BLUE='\033[1;34m'   # 蓝
PINK='\033[1;35m'   # 紫
CYAN='\033[1;36m'   # 青
WHITE='\033[1;37m'  # 白
CLS='\033[0m'       # 清除颜色

INFO="${GREEN}[INFO]${CLS}"
WARN="${YELLOW}[WARN]${CLS}"
ERR="${RED}[ERR ]${CLS}"

SCRIPT_NAME=${0#*/}
SCRIPT_CURRENT_PATH=${0%/*}
SCRIPT_ABSOLUTE_PATH=`cd $(dirname ${0}); pwd`

SYSTEM_ENVIRONMENT_FILE=/etc/profile # 系统环境变量位置
USER_ENVIRONMENT_FILE=~/.bashrc
SOFTWARE_DIR_PATH=~/2software        # 软件安装目录
# * ======================================================

XINETD_CONF_NAME=xinetd.conf
XINETD_CONF_PATH=/etc/${XINETD_CONF_NAME}

TFTPD_HPA_CONFIG_NAME=tftpd-hpa
TFTPD_HPA_CONFIG_PATH=/etc/default/${TFTPD_HPA_CONFIG_NAME}

TFTPD_CONFIG_NAME=tftp
TFTPD_XINETD_DIR_PATH=/etc/xinetd.d
TFTPD_CONFIG_PATH=${TFTPD_XINETD_DIR_PATH}/${TFTPD_CONFIG_NAME}

TFTP_CFG_SRC_PATH=${SCRIPT_ABSOLUTE_PATH}      # tftp安装所需文件所在目录
TFTP_SERVICE_DIR=~/3tftp                       # TFTP服务器目录,配置文件已写死，这里需要固定为这个目录

function tftp_install_online()
{
    cd ~
    echo -e "${INFO}${PINK}current path:$(pwd)${CLS}"
	echo -e "${INFO}${PINK}XINETD_CONF_NAME:${XINETD_CONF_NAME}${CLS}"
	echo -e "${INFO}${PINK}TFTP_CFG_SRC_PATH  :${TFTP_CFG_SRC_PATH}${CLS}"
	echo -e "${INFO}${PINK}XINETD_CONF_PATH:${XINETD_CONF_PATH}${CLS}"
	echo -e "${INFO}${PINK}TFTPD_HPA_CONFIG_PATH:${TFTPD_HPA_CONFIG_PATH}${CLS}"
	echo -e "${INFO}${PINK}TFTPD_CONFIG_PATH:${TFTPD_CONFIG_PATH}${CLS}"

	tftpd_hpa_version=$(dpkg -s tftpd-hpa | awk -F: '$0 ~ "Version"')
	if [ "${tftpd_hpa_version}" == "" ];then
		echo -e "${ERR}tftp 服务尚未安装..."
		echo -e "${INFO}安装 xinetd ..."
		sudo apt-get install -y xinetd
		if [ ! -e "${TFTP_CFG_SRC_PATH}${XINETD_CONF_PATH}" ];then
			echo -e "${ERR}${TFTP_CFG_SRC_PATH}${XINETD_CONF_PATH} 不存在!!!"
			return
		fi
		echo -e "${INFO}准备拷贝 /etc/xinetd.conf 文件到 ubuntu ..."
		sudo cp -prv ${TFTP_CFG_SRC_PATH}${XINETD_CONF_PATH} ${XINETD_CONF_PATH}
		
		echo -e "${INFO}创建tftp相关目录 ..."
		mkdir -pv ${TFTP_SERVICE_DIR}
		sudo chmod 777 ${TFTP_SERVICE_DIR}
		
		echo -e "${INFO}安装 tftp-hpa tftpd-hpa ..."
		sudo apt-get install -y tftp-hpa tftpd-hpa
		if [ ! -e "${TFTP_CFG_SRC_PATH}${TFTPD_HPA_CONFIG_PATH}" ];then
			echo -e "${ERR}${TFTPD_HPA_CONFIG_PATH} 不存在!!!"
			return
		fi
		echo -e "${INFO}准备拷贝 /etc/default/tftpd-hpa 文件到 ubuntu ..."
		sudo cp -prv ${TFTP_CFG_SRC_PATH}${TFTPD_HPA_CONFIG_PATH} ${TFTPD_HPA_CONFIG_PATH}

		if [ ! -e "${TFTP_CFG_SRC_PATH}${TFTPD_CONFIG_PATH}" ];then
			echo -e "${ERR}${TFTPD_CONFIG_PATH}不存在!!!"
		fi
		echo -e "${INFO}准备拷贝 /etc/xinetd.d/tftp 文件到 ubuntu ..."
		if [ ! -d "/etc/xinetd.d" ];then
			echo -e "${WARN} /etc/xinetd.d 不存在, 即将创建..."
			sudo mkdir -pv /etc/xinetd.d
		fi
		sudo cp -prv ${TFTP_CFG_SRC_PATH}${TFTPD_CONFIG_PATH} ${TFTPD_CONFIG_PATH}
		
		echo -e "${INFO}重启相关服务..."
		sudo service tftpd-hpa restart # 重启 tftpd-hpa
		sudo service xinetd restart    # 重启 xinetd

		echo -e "${INFO}tftp 安装完毕,当前版本如下:"
	else
		echo -e "${INFO}tftp 已安装,当前版本如下:"
	fi
	tftpd_hpa_version=$(dpkg -s tftpd-hpa | awk -F: '$0 ~ "Version"')
	echo ${tftpd_hpa_version}
	
	echo -e "${INFO}测试步骤如下:"
	echo "cd /home/hk/3tftp   # 进入服务器工作目录"
	echo "sudo touch test.txt # 创建测试文件"
	echo "cd ~                # 回到家目录"
	echo "tftp localhost      # tftp登录本机"
	echo "tftp> get test.txt  # tftp获取测试文件"
	echo "tftp> quit          # tftp退出"
	echo "ls                  # 查看是否获取成功"
	
}
function debug_info()
{
    echo "SCRIPT_NAME=${SCRIPT_NAME}"
    echo "SCRIPT_CURRENT_PATH=${SCRIPT_CURRENT_PATH}"
    echo "SCRIPT_ABSOLUTE_PATH=${SCRIPT_ABSOLUTE_PATH}"
    echo "TFTP_CFG_SRC_PATH=${TFTP_CFG_SRC_PATH}"
}

tftp_install_online