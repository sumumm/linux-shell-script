#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : nfs.sh
# * Author     : 苏木
# * Date       : 2024-09-21
# * ======================================================
##

BLACK="\033[1;30m"
RED='\033[1;31m'    # 红
GREEN='\033[1;32m'  # 绿
YELLOW='\033[1;33m' # 黄
BLUE='\033[1;34m'   # 蓝
PINK='\033[1;35m'   # 紫
CYAN='\033[1;36m'   # 青
WHITE='\033[1;37m'  # 白
CLS='\033[0m'       # 清除颜色

INFO="${GREEN}[INFO]${CLS}"
WARN="${YELLOW}[WARN]${CLS}"
ERR="${RED}[ERR ]${CLS}"

SCRIPT_NAME=${0#*/}
SCRIPT_CURRENT_PATH=${0%/*}
SCRIPT_ABSOLUTE_PATH=`cd $(dirname ${0}); pwd`

SYSTEM_ENVIRONMENT_FILE=/etc/profile # 系统环境变量位置
USER_ENVIRONMENT_FILE=~/.bashrc
SOFTWARE_DIR_PATH=~/2software        # 软件安装目录
# * ======================================================

NFS_SERVER_DIR_NAME=4nfs
NFS_SERVER_DIR_PATH=~/${NFS_SERVER_DIR_NAME}

NFS_EXPORTS_FILE_PATH=/etc/exports
NFS_EXPORTS_FILE_TMP_PATH=${SCRIPT_ABSOLUTE_PATH}${NFS_EXPORTS_FILE_PATH}

NFS_KERNEL_SERVER_FILE_PATH=/etc/default/nfs-kernel-server
NFS_KERNEL_SERVER_FILE_TMP_PATH=${SCRIPT_ABSOLUTE_PATH}${NFS_KERNEL_SERVER_FILE_PATH}

NFS_CONF_FILE_PATH=/etc/nfs.conf
NFS_CONF_FILE_TMP_PATH=${SCRIPT_ABSOLUTE_PATH}${NFS_CONF_FILE_PATH}

# 这里只是记录一下相关命令，后续作为参考，是从之前的脚本中摘出来的，所以运行不了
function nfs_install()
{

    cd ~
    echo -e "${INFO}${PINK}current path:$(pwd)${CLS}"
	
	nfs_version=$(dpkg -s nfs-kernel-server | awk -F: '$0 ~ "Version"')
	if [ "${nfs_version}" == "" ];then
		echo -e "${ERR}nfs 服务尚未安装..."
		
		echo -e "${INFO}安装 nfs-kernel-server ..."
		sudo apt-get install -y nfs-kernel-server

        if [ ! -d ${NFS_SERVER_DIR_PATH} ];then
            echo -e "${INFO}创建nfs相关目录 ..."
            sudo mkdir -p ${NFS_SERVER_DIR_PATH} 
		    sudo chmod -R 777 ${NFS_SERVER_DIR_PATH}
        else
            echo "${NFS_SERVER_DIR_PATH} 路径已存在..."
        fi

		if [ ! -e "${NFS_EXPORTS_FILE_TMP_PATH}" ];then
			echo -e "${ERR}${NFS_EXPORTS_FILE_TMP_PATH}不存在!!!"
            ##sed命令添加
            NFS_SERVER_DIR_PATH=$(readlink -f ~/4nfs)
            echo -e "${INFO}添加配置参数..."
            config_data="${NFS_SERVER_DIR_PATH} *(rw,sync,no_subtree_check,no_root_squash)"
            if test -s "/etc/exports" ;then # 文件存在且至少有一个字符为真
                sudo sed -i "\$a $config_data" /etc/exports
            else
                sudo echo "$config_data" >> /etc/exports
            fi
            
            config_data="RPCNFSDOPTS=\"--nfs-version 2,3,4 --debug --syslog\""
            if test -s "/etc/exports" ;then # 文件存在且至少有一个字符为真
                sudo sed -i "\$a $config_data" /etc/default/nfs-kernel-server
            else
                sudo echo "$config_data" >> /etc/default/nfs-kernel-server
            fi
        else
            echo -e "${INFO}${NFS_EXPORTS_FILE_TMP_PATH}存在!!!"
            sudo cp -pvf ${NFS_EXPORTS_FILE_TMP_PATH} ${NFS_EXPORTS_FILE_PATH}
		fi
        
        # 支持nfs 2.0版本 
        sudo cp -pvf ${NFS_KERNEL_SERVER_FILE_TMP_PATH} ${NFS_KERNEL_SERVER_FILE_PATH}
        sudo cp -pvf ${NFS_CONF_FILE_TMP_PATH} ${NFS_CONF_FILE_PATH}

		echo -e "${INFO}重启nfs相关服务..."
		sudo /etc/init.d/rpcbind restart
		sudo /etc/init.d/nfs-kernel-server restart
		
		echo -e "${INFO}nfs 安装完毕,当前版本如下:"
	else
		echo -e "${INFO}nfs 已安装,当前版本如下:"
	fi
	nfs_version=$(dpkg -s nfs-kernel-server | awk -F: '$0 ~ "Version"')
	echo ${nfs_version}
	
    echo -e "${INFO}nfs支持的所有版本如下:"
    sudo cat /proc/fs/nfsd/versions

	echo -e "${INFO}测试步骤如下:"
	echo "cd ~/4nfs           # 进入nfs工作目录"
	echo "sudo touch test.txt # 创建测试文件"
	echo "cd ~                # 回到家目录"
	echo "sudo mkdir -p /mnt/nfs_temp # 创建挂载目录"
	echo "sudo mount -t nfs localhost:${SERVER_TFTP_PATH} /mnt/nfs_temp # 挂载nfs" 
	echo "ls /mnt/nfs_temp    # 查看是否挂载成功"
	
}

nfs_install