#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : samba.sh
# * Author     : 苏木
# * Date       : 2024-09-28
# * ======================================================
##

BLACK="\033[1;30m"
RED='\033[1;31m'    # 红
GREEN='\033[1;32m'  # 绿
YELLOW='\033[1;33m' # 黄
BLUE='\033[1;34m'   # 蓝
PINK='\033[1;35m'   # 紫
CYAN='\033[1;36m'   # 青
WHITE='\033[1;37m'  # 白
CLS='\033[0m'       # 清除颜色

INFO="${GREEN}[INFO]${CLS}"
WARN="${YELLOW}[WARN]${CLS}"
ERR="${RED}[ERR ]${CLS}"

SCRIPT_NAME=${0#*/}
SCRIPT_CURRENT_PATH=${0%/*}
SCRIPT_ABSOLUTE_PATH=`cd $(dirname ${0}); pwd`

SYSTEM_ENVIRONMENT_FILE=/etc/profile # 系统环境变量位置
USER_ENVIRONMENT_FILE=~/.bashrc
SOFTWARE_DIR_PATH=~/2software        # 软件安装目录
# * ======================================================
8linux-shell-script/samba/etc/smb.conf
SMB_CONF_DIR_PATH=/etc/samba
SMB_CONF_FILE_PATH=/etc/samba/smb.conf
SMB_CONF_FILE_TMP_PATH=${SCRIPT_ABSOLUTE_PATH}${SMB_CONF_FILE_PATH}

# 打印菜单
function samba_echo_menu()
{
	echo "================================================="
	echo -e "${GREEN}               samba服务 ${CLS}"
	echo "================================================="
	echo -e "${PINK}current path    :$(pwd)${CLS}"
	echo ""
	echo -e "${GREEN} *[0]${CLS} 在线安装samba服务"
	echo ""
	echo "================================================="
}

function samba_install()
{
	echo -e "${INFO}${PINK}current path:$(pwd)${CLS}"
	echo -e "${INFO}${PINK}SMB_CONF_PATH:${SMB_CONF_PATH}${CLS}"

	sudo apt-get install -y samba samba-common

    if [ ! -d ${SMB_CONF_DIR_PATH} ];then
        echo "${SMB_CONF_DIR_PATH} 路径不存在，正在创建..."
	    sudo mkdir -pv ${SMB_CONF_DIR_PATH}
    else
    	echo "${SMB_CONF_DIR_PATH} 路径已存在..."
    fi
	
    echo -e "${INFO}拷贝samba配置文件，需要注意修改里面的path为要共享的目录..."
	sudo cp -pvf ${SMB_CONF_FILE_TMP_PATH} ${SMB_CONF_FILE_PATH}
	sudo smbpasswd -a sumu # 设置访问密码
	#sudo /etc/init.d/smbd restart
    # 注意：当输入第一条命令回车后，会提示输入密码，输入虚拟机的用户密码回车即可。
    #      之后输入第二条命令，会连续三次弹出这个窗口，也就是说，输入以上两条命令，
    #      前后需要输入四次虚拟机的密码（注意：不是 Samba 用户的密码）。
    echo -e "${INFO}Samba服务重启(将会有四次需要输入ubuntu的密码)"
    systemctl restart smbd.service
    systemctl enable smbd.service

    echo ${INFO}"注意:windows下要开启SMB功能, 控制面板->程序->启用或关闭Windows功能,找到并勾选SMB 1.0/CIFS文件共享支持"
    # 查看当前 Samba 服务器的运行情况。
    echo -e "${INFO}Samba运行情况如下:"
    systemctl status smbd.service


    # smb.conf的内容如下时：
    # [ubuntu-samba]
    # path = /home/hk/5ALPHA
    # available = yes
    # browseable = yes             
    # public = yes               
    # writable = yes
    # valid users = sumu
    # 我们在windows下使用 \\<ubuntu IP>\ubuntu-samba 来访问samba共享目录
}

function samba_func_process()
{
    samba_echo_menu
	read -p "请选择功能，默认选择退出:" choose
	case "${choose}" in
		"0") samba_install;;
		*)  exit 0;;
	esac
}

samba_func_process