#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : git.sh
# * Author     : 苏木
# * Date       : 2024-09-28
# * ======================================================
##

BLACK="\033[1;30m"
RED='\033[1;31m'    # 红
GREEN='\033[1;32m'  # 绿
YELLOW='\033[1;33m' # 黄
BLUE='\033[1;34m'   # 蓝
PINK='\033[1;35m'   # 紫
CYAN='\033[1;36m'   # 青
WHITE='\033[1;37m'  # 白
CLS='\033[0m'       # 清除颜色

INFO="${GREEN}[INFO]${CLS}"
WARN="${YELLOW}[WARN]${CLS}"
ERR="${RED}[ERR ]${CLS}"

SCRIPT_NAME=${0#*/}
SCRIPT_CURRENT_PATH=${0%/*}
SCRIPT_ABSOLUTE_PATH=`cd $(dirname ${0}); pwd`

SYSTEM_ENVIRONMENT_FILE=/etc/profile # 系统环境变量位置
USER_ENVIRONMENT_FILE=~/.bashrc
SOFTWARE_DIR_PATH=~/2software        # 软件安装目录

# 打印菜单
function git_echo_menu()
{
	echo "================================================="
	echo -e "${GREEN}               Git工具安装与配置 ${CLS}"
	echo "================================================="
	echo -e "${PINK}current path        :$(pwd)${CLS}"
    echo -e "${PINK}SCRIPT_ABSOLUTE_PATH:${SCRIPT_ABSOLUTE_PATH}${CLS}"
	echo ""
	echo -e "${GREEN} *[0]${CLS} 安装Git"
    echo -e "${GREEN} *[1]${CLS} 配置Git"
    echo -e "${GREEN} *[2]${CLS} 安装配置Git,一步到位(适合新装的linux)"
	echo ""
	echo "================================================="
}

# 安装 git
function git_install_online()
{
    cd ~
    echo -e "${INFO}current path:$(pwd)"
    echo -e "${INFO}检查git是否安装..."
    if ! $(command -v git >/dev/null 2>&1) ; then
    	echo -e "${ERR}git 尚未安装!!!"
        read -p "是否安装git?(默认选择继续,退出请输入n):" ret
        if [ "${ret}" != "n" ];then
            sudo add-apt-repository ppa:git-core/ppa
            sudo apt update
            sudo apt-get install -y git-core
            echo -e "${INFO}安装完毕,当前git版本如下:"
        else
            return
        fi
    else
        echo -e "${INFO}git 已安装,当前git版本如下:"
    fi

    git_version=$(git -v | sed -n "1p")
    echo ${git_version}
}

# 配置git
function git_ssh_config()
{
    cd ~
    echo -e "${INFO}current path:$(pwd)"
    # 检查Git是否安装
    if ! $(command -v git >/dev/null 2>&1) ; then
    	echo -e "${ERR}git 尚未安装,请先安装git!!!"
        return
    else
        echo -e "${INFO}git 已安装,当前git版本如下:"
    fi
    git_version=$(git -v | sed -n "1p")
    echo ${git_version}

    read -p "是否需要配置git基本信息?(默认选择继续,不需要请输入n):" ret
    if [ "${ret}" != "n" ];then
        echo -e "${INFO}开始配置git基本信息..."
        read -p "请输入 gitee 用户名: " giteeuser
        read -p "请输入 gitee 邮箱: " giteeemail
        git config --global user.name "${giteeuser}"
        git config --global user.email "${giteeemail}"
        git config --global core.autocrlf false
        # git config --global core.editor vim # 需要提前安装好vim
        git config --global alias.mylog "log --graph --pretty=format:'%C(magenta)[%an]%C(reset) %C(red)%h%C(reset) : %C(green)%s%C(reset) %C(yellow)%d%C(reset) %C(blue)(%cr)%C(reset)' --abbrev-commit --date=format:'%Y-%m-%d %H:%M:%S'"
        echo -e "${INFO}git基本信息完成,配置项如下："
        git config --list --show-origin
    fi
    
    read -p "是否需要配置SSH?(默认选择继续,不需要请输入n):" ret
    if [ "${ret}" != "n" ];then
        echo -e "${INFO}开始检查SSH(以Gitee为例)..."
        ssh -T git@gitee.com
        read -p "输出是否含有: git@gitee.com: Permission denied (publickey). ?(y or n)" ret
        if [ "${ret}" = "y" ] || [ "${ret}" = "" ]; then
            echo -e "${ERR}SSH密钥未安装,无法连接到 gitee,需要配置SSH!!!"
        else
            echo -e "${INFO}SSH密钥已安装,可正常使用!!!"
            read -p "是否重新安装 SSH ?(默认退出,继续输入y)" ret
            if [ "${ret}" != "y" ]; then
                return
            else
                rm -rvf ~/.ssh
            fi
        fi
        echo -e "${INFO}开始配置SSH(gitee)..."
        read -p "请输入一个标识，可以是邮箱:" sshkey
        ssh-keygen -t ed25519 -C ${sshkey} # 只是生成的 sshkey 的名称，并不约束或要求具体命名为某个邮箱
        echo -e "${INFO}即将生成sshkey,不需要特别需求时可直接依提示按三次回车!!!"
        echo -e "${INFO}即将打开sshkey,复制后可按 q 返回..."
        sleep 2s
        less ~/.ssh/id_ed25519.pub
        echo -e "${INFO}配置完成,请将sshkey添加到gitee或者github!"
        echo -e "${INFO}【Github】→【登录账号】→【头像】→【Setting】→【SSH and GPG keys 】→【SSH keys】→【New SSH key】"
        echo -e "${INFO}【Gitee】→【登录账号】→【头像】→【账号设置】→【安全设置】→【SSH公钥】→【添加公钥】"
        echo -e "${INFO}https://github.com/settings/keys"
        echo -e "${INFO}https://gitee.com/profile/sshkeys"
        sleep 2s
    fi
}

function git_install_cfg()
{
    git_install_online
    git_ssh_config
}

function git_func_process()
{
    git_echo_menu
	read -p "请选择功能，默认选择退出:" choose
	case "${choose}" in
		"0") git_install_online;;
		"1") git_ssh_config;;
		"2") git_install_cfg;;
		*)  exit 0;;
	esac
}

git_func_process
