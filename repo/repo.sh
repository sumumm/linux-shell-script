#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : repo.sh
# * Author     : 苏木
# * Date       : 2024-09-20
# * ======================================================
##

BLACK="\033[1;30m"
RED='\033[1;31m'    # 红
GREEN='\033[1;32m'  # 绿
YELLOW='\033[1;33m' # 黄
BLUE='\033[1;34m'   # 蓝
PINK='\033[1;35m'   # 紫
CYAN='\033[1;36m'   # 青
WHITE='\033[1;37m'  # 白
CLS='\033[0m'       # 清除颜色

INFO="${GREEN}[INFO]${CLS}"
WARN="${YELLOW}[WARN]${CLS}"
ERR="${RED}[ERR ]${CLS}"

SCRIPT_NAME=${0#*/}
SCRIPT_CURRENT_PATH=${0%/*}
SCRIPT_ABSOLUTE_PATH=`cd $(dirname ${0}); pwd`

SYSTEM_ENVIRONMENT_FILE=/etc/profile # 系统环境变量位置
USER_ENVIRONMENT_FILE=~/.bashrc
SOFTWARE_DIR_PATH=~/2software        # 软件安装目录
# * ======================================================

# curl https://mirrors.tuna.tsinghua.edu.cn/git/git-repo -o ./repo_bin/repo # 国内站点
# curl https://storage.googleapis.com/git-repo-downloads/repo > ./repo_bin/repo # 国外站点

# chmod a+x ./repo_bin/repo
# cp -prvf repo_bin ~/2software

# echo "export PATH=~/2software/repo_bin:$PATH" >> ~/.bashrc
# source ~/.bashrc
function repo_install()
{
    REPO_CMD_PACK_PATH=${SCRIPT_ABSOLUTE_PATH}/repo_bin
	REPO_CMD_INSTALL_PATH=${SOFTWARE_DIR_PATH}/repo_bin

    cd ~
    echo -e "${INFO}${PINK}current path          :$(pwd)${CLS}"
	echo -e "${INFO}${PINK}REPO_CMD_PACK_PATH    :${REPO_CMD_PACK_PATH}${CLS}"
	echo -e "${INFO}${PINK}REPO_CMD_INSTALL_PATH :${REPO_CMD_INSTALL_PATH}${CLS}"
	# repo其实是一个python脚本
	if [ ! -e "${REPO_CMD_INSTALL_PATH}/repo" ]; then
    	echo -e "${ERR}repo 尚未安装,准备安装repo..."
		echo -e "${INFO}创建安装目录..."
		if [ -d "${REPO_CMD_INSTALL_PATH}" ];then
			echo -e "${WARN}${REPO_CMD_INSTALL_PATH}目录已存在!!!"
			read -p "是否删除?(默认选择删除继续安装，退出安装输入n):" ret
			if [ "${ret}" != "n" ];then
				sudo rm -rf ${REPO_CMD_INSTALL_PATH}
			else
				return
			fi
		else
			echo -e "${INFO}即将创建${REPO_CMD_INSTALL_PATH}安装目录..."
		fi
		sudo mkdir -pv ${REPO_CMD_INSTALL_PATH}
        sudo chmod 777 ${REPO_CMD_INSTALL_PATH}
		echo -e "${INFO}下载 repo 安装包..."
		if [ "$(command -v curl >/dev/null 2>&1)" ] ; then
			echo -e "${ERR}curl 尚未安装,开始安装curl...!!!"
			sudo apt-get install -y curl
		else
			echo -e "${INFO}curl 已安装。"
		fi
        echo -e "${INFO}curl 版本如下："
        curl_version=$(curl --version | sed -n "1p")
		echo ${curl_version}
		
        # curl https://mirrors.tuna.tsinghua.edu.cn/git/git-repo -o ./repo_bin/repo # 国内站点
        sudo curl https://storage.googleapis.com/git-repo-downloads/repo > ${REPO_CMD_INSTALL_PATH}/repo # 国外站点
        chmod a+x ${REPO_CMD_INSTALL_PATH}/repo

		echo -e "${INFO}添加环境变量..."
		local REPO_CMD_PATH_STR="${REPO_CMD_INSTALL_PATH}"

		# 判断匹配函数，匹配函数不为0，则包含给定字符
		ENVIRONMENT_FILE_PATH=${USER_ENVIRONMENT_FILE}
		if [ `grep -c "${REPO_CMD_PATH_STR}" ${ENVIRONMENT_FILE_PATH}` -ne '0' ];then
			echo -e "${INFO}文件中已包含 ${REPO_CMD_PATH_STR}，不需要重新写入!"
		else
			environment_data="export PATH=\$PATH:${REPO_CMD_PATH_STR}"
			if test -s ${ENVIRONMENT_FILE_PATH} ;then # 文件存在且至少有一个字符为真
				sudo sed -i "\$a $environment_data" ${ENVIRONMENT_FILE_PATH}
			else
				echo "$environment_data" >> ${ENVIRONMENT_FILE_PATH}
			fi
			source ${ENVIRONMENT_FILE_PATH}
			echo -e "${INFO}安装完毕(为确保后续没有问题，请重启ubuntu)..."
		fi
    else
        echo -e "${INFO}repo 已安装."
    fi
	
	# 这里有可能出现未找到命令的情况，重启下终端即可
	repo help
}

repo_install