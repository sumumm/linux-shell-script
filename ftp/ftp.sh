#!/bin/bash
# * =====================================================
# * Copyright © hk. 2022-2025. All rights reserved.
# * File name  : ftp.sh
# * Author     : 苏木
# * Date       : 2024-09-20
# * ======================================================
##

sudo apt-get update         # 更新
sudo apt-get install vsftpd # 安装ftp服务

#sudo vim /etc/vsftpd.conf   # 修改配置文件，直接用下面的命令拷贝覆盖
sudo cp -pvf ./etc/vsftpd.conf /etc/vsftpd.conf

sudo /etc/init.d/vsftpd restart      # 重启服务
ps -aux | grep vsftpd | grep -v grep # 打印服务启动情况
